package com.lecuong.shop.dao;

import com.lecuong.shop.entity.HoaDon;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class HoaDonDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Transactional
    public int themHoaDon(HoaDon hoaDon){

        Session session = sessionFactory.getCurrentSession();

        int id = (int) session.save(hoaDon);

        if (0 < id){
            return id;
        }else{
            return 0;
        }
    }
}
