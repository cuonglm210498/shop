package com.lecuong.shop.entity;

import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "sanpham")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SanPham {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idsanpham")
    private int idSanPham;

    @Column(name = "tensanpham")
    private String tenSanPham;

    @Column(name = "giatien")
    private String giaTien;

    @Column(name = "mota")
    private String moTa;

    @Column(name = "hinhsanpham")
    private String hinhSanPham;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "iddanhmucsanpham")
    private DanhMucSanPham danhMucSanPham;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "idsanpham")
    private Set<ChiTietSanPham> chiTietSanPham;

    //    @OneToMany(cascade = CascadeType.ALL, mappedBy = "sanPham")
    //    private Set<ChiTietSanPham> chiTietSanPham;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "chitietkhuyenmai",
            joinColumns = {@JoinColumn(name = "idsanpham", referencedColumnName = "idsanpham")},
            inverseJoinColumns = {@JoinColumn(name = "idkhuyenmai", referencedColumnName = "idkhuyenmai")})
    private Set<KhuyenMai> danhSachKhuyenMai;
}
