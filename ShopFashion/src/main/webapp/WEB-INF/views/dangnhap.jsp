<!--Author: Le Manh Cuong, Date: 11/08/2020 10:38AM -->
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href='<c:url value="css/styles.css" />' />
    <link href='<c:url value="fonts/Roboto.css" />' rel='stylesheet' />
    <script src='<c:url value="js/jquery-3.5.1.min.js" />'></script>
    <script src='<c:url value="js/custom.js" />'></script>
    <title>Đăng nhập</title>
</head>
<body id="body-login">
<div id="body-flex-login">
    <div id="container-login">
        <div id="container-login-left">
            <div id="header-top-left" class="header-login">
                <span id="text-logo">Welcome</span><br/>
                <span id="hint-text-logo">Hãy tạo nên phong cách của bạn cùng Lê Cường shop</span>
            </div>

            <div id="header-bottom-left">
                <p>
                    <img src='<c:url value="images/icon_like.png" />' alt="" width="15px" height="15px">
                    <span>Luôn cập nhật xu hướng thời trang mới nhất.</span>
                </p>
                <p>
                    <img src='<c:url value="images/icon_like.png" />' alt="" width="15px" height="15px">
                    <span>Giảm hơn 50% tất cả các mặt hàng giành cho khách VIP.</span>
                </p>
                <p>
                    <img src='<c:url value="images/icon_like.png" />' alt="" width="15px" height="15px">
                    <span>Tận tình tư vấn để tạo nên phong cách của bạn.</span>
                </p>
            </div>
        </div>

        <div id="container-login-right">
            <div id="header-top-right" class="header-login" style="cursor: pointer;">
                <span class="actived" id="dangnhap">Đăng nhập</span> / <span id="dangky">Đăng ký</span>
            </div>

            <div id="container-center-login-right">
                <div class="container-login-form" id="container-center-login-right">
                    <input id="email" name="email" class="material-textinput input-icon-email" type="text" placeholder="Email" /><br/>
                    <input id="matkhau" name="matkhau" class="material-textinput input-icon-password" type="password" placeholder="Mật khẩu" style="margin-top: 8px;" /><br/>
                    <input id="btnDangNhap" class="material-primary-button" type="submit" value="Đăng nhập" /><br/>
                </div>

                <div class="container-signup-form" id="container-center-login-right" style="display: none;">
                    <form action="/dangnhap" method="post">
                        <input id="email" name="email" class="material-textinput input-icon-email" type="text" placeholder="Email" /><br/>
                        <input id="matkhau" name="matkhau" class="material-textinput input-icon-password" type="password" placeholder="Mật khẩu" style="margin-top: 8px;" /><br/>
                        <input id="nhaplaimatkhau" name="nhaplaimatkhau" class="material-textinput input-icon-password" type="password" placeholder="Nhập lại mật khẩu" style="margin-top: 8px;" /><br/>
                        <input id="btnDangNhap" class="material-primary-button" type="submit" value="Đăng ký" /><br/>
                    </form>
                </div>
            </div>

            <div style="margin-top: 10px; text-align: center; font-size: 18px;}">
                <span id="ketqua">${kiemtradangnhap}</span>
            </div>

            <div id="container-social-login">
                <img src='<c:url value="images/icon_facebook.png" />' />
                <img src='<c:url value="images/icon_google.png" />' />
            </div>
        </div>
    </div>
</div>
</body>
</html>