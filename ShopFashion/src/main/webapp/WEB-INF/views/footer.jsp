<!--Author: Le Manh Cuong, Date: 11/08/2020 10:38AM -->
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href='<c:url value="css/styles.css" />' />
    <!-- <link rel="stylesheet" href="./css/bootstrap.min.css" /> -->
    <script src='<c:url value="js/jquery-3.5.1.min.js" />'></script>
    <link rel="stylesheet" href='<c:url value="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" />' />
    <script src='<c:url value="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" />'></script>
    <script src='<c:url value="https://code.jquery.com/jquery-3.2.1.slim.min.js" />'></script>
    <script src='<c:url value="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" />'></script>
    <link href='<c:url value="fonts/Roboto.css" />' rel='stylesheet' />
    <link rel="stylesheet" href='<c:url value="css/animate.css" />' />
    <script src='<c:url value="js/wow.min.js" />'></script>
    <script>new WOW().init();</script>
    <title>Document</title>
</head>
<body>
    <div id="footer" class="container-fluid">
        <div class="row">
            <div class="col-sm-4 col-md-4">
                <p><span class="title-footer">THÔNG TIN SHOP</span></p>
                <span>Shop Lê Cường là một thương hiệu thời trang đầy uy tín, luôn đảm bảo chất lượng sản phẩm tốt nhất cho khách hàng</span>
            </div>

            <div class="col-sm-4 col-md-4">
                <p><span class="title-footer">LIÊN HỆ</span></p>
                <p><span>Địa chỉ: Thị trấn Mỹ Lộc, huyện Mỹ Lộc, tỉnh Nam Định</span></p><br>
                <p><span>Email: lemanhcuong0498@gmail.com</span></p><br>
                <p><span>Số điện thoại: 0912345678</span></p>
            </div>

            <div class="col-sm-4 col-md-4">
                <p><span class="title-footer">GÓP Ý</span></p>
                <form action="" method="post">
                    <input name="" type="text" placeholder="Email"><br>
                    <textarea name="" id="" cols="50" rows="4" placeholder="Nội dung"></textarea>
                    <button style="color: white;" class="material-primary-button" type="submit" value="submit" name="">GỬI</button>
                </form>
            </div>
        </div>
    </div>
</body>
</html>