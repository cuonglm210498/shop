package com.lecuong.shop.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "mausanpham")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MauSanPham {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idmausanpham")
    private int idMauSanPham;

    @Column(name = "tenmau")
    private String tenMau;
}
