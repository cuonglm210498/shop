package com.lecuong.shop.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "chitietsanpham")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ChiTietSanPham {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idchitietsanpham")
    private int idChiTietSanPham;

    @Column(name = "soluong")
    private int soLuong;

    @Column(name = "ngaynhap")
    private String ngayNhap;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "idsanpham")
    private SanPham sanPham;

//    @ManyToOne
//    @JoinColumn(name = "idsanpham")
//    private SanPham sanPham;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "idsizesanpham")
    private SizeSanPham sizeSanPham;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "idmausanpham")
    private MauSanPham mauSanPham;
}
