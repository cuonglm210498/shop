package com.lecuong.shop.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "chitiethoadon")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ChiTietHoaDon {

    @EmbeddedId
    private ChiTietHoaDonId chiTietHoaDonId;

    @Column(name = "soluong")
    private int soLuong;

    @Column(name = "giatien")
    private String giaTien;

}
