package com.lecuong.shop.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "nhanvien")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class NhanVien {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idnhanvien")
    private int idNhanVien;

    @Column(name = "hoten")
    private String hoTen;

    @Column(name = "diachi")
    private String diaChi;

    @Column(name = "gioitinh")
    private boolean gioiTinh;

    @Column(name = "cmnd")
    private String cmnd;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "idChucVu")
    private ChucVu chucVu;

    @Column(name = "email")
    private String email;

    @Column(name = "tendangnhap")
    private String tenDangNhap;

    @Column(name = "matkhau")
    private String matKhau;
}
