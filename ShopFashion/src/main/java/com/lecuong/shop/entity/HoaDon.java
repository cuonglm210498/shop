package com.lecuong.shop.entity;

import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "hoadon")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class HoaDon {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idhoadon")
    private int idHoaDon;

    @Column(name = "tenkhachhang")
    private String tenKhachHang;

    @Column(name = "sodienthoai")
    private String soDienThoai;

    @Column(name = "diachigiaohang")
    private String diaChiGiaoHang;

    @Column(name = "tinhtrang")
    private boolean tinhTrang;

    @Column(name = "ngaylap")
    private String ngayLap;

    @Column(name = "hinhthucgiaohang")
    private String hinhThucGiaoHang;

    @Column(name = "ghichu")
    private String ghiChu;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "idhoadon")
    private Set<ChiTietHoaDon> danhSachChiTietHoaDon;
}
