package com.lecuong.shop.controller;

import com.lecuong.shop.entity.GioHang;
import com.lecuong.shop.service.NhanVienService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/api")
@SessionAttributes({"user","giohang"})
public class APIController {

    @Autowired
    private NhanVienService nhanVienService;

    @PostMapping("/kiemtradangnhap")
    @ResponseBody
    public String kiemTraDangNhap(@RequestParam String email,
                                  @RequestParam String matkhau,
                                  ModelMap modelMap) {

        boolean kiemTra = nhanVienService.kiemTraDangNhap(email, matkhau);

        modelMap.addAttribute("user", email);

        return String.valueOf(kiemTra);
    }

    /*Lam gio hang*/
    @GetMapping("/themgiohang")
    @ResponseBody
    public String themGioHang(@RequestParam int idSanPham,
                            @RequestParam int idSize,
                            @RequestParam int idMau,
                            @RequestParam String tenSanPham,
                            @RequestParam String giaTien,
                            @RequestParam String tenMau,
                            @RequestParam String tenSize,
                            @RequestParam int soLuong,
                            @RequestParam int idChiTietSanPham,
                            HttpSession httpSession) {

        if (null == httpSession.getAttribute("giohang")) {
            List<GioHang> gioHangs = new ArrayList<>();

            GioHang gioHang = new GioHang();
            gioHang.setIdSanPham(idSanPham);
            gioHang.setIdSize(idSize);
            gioHang.setIdMau(idMau);
            gioHang.setTenSanPham(tenSanPham);
            gioHang.setGiaTien(giaTien);
            gioHang.setTenMau(tenMau);
            gioHang.setTenSize(tenSize);
            gioHang.setSoLuong(1);
            gioHang.setIdChiTietSanPham(idChiTietSanPham);

            gioHangs.add(gioHang);
            httpSession.setAttribute("giohang", gioHangs);

            return gioHangs.size()+"";

        } else {
            List<GioHang> listGioHang = (List<GioHang>) httpSession.getAttribute("giohang");

            int viTri = kiemTraSanPhamDaTonTaiGioHang(listGioHang, idSanPham, idSize, idMau);
            if (viTri == -1) {
                GioHang gioHang = new GioHang();
                gioHang.setIdSanPham(idSanPham);
                gioHang.setIdSize(idSize);
                gioHang.setIdMau(idMau);
                gioHang.setTenSanPham(tenSanPham);
                gioHang.setGiaTien(giaTien);
                gioHang.setTenMau(tenMau);
                gioHang.setTenSize(tenSize);
                gioHang.setSoLuong(1);
                gioHang.setIdChiTietSanPham(idChiTietSanPham);

                listGioHang.add(gioHang);
                gioHang.setIdChiTietSanPham(idChiTietSanPham);
            } else {
                int soLuongMoi = listGioHang.get(viTri).getSoLuong() + 1;
                listGioHang.get(viTri).setSoLuong(soLuongMoi);
            }
            return listGioHang.size()+"";
        }
    }

    private int kiemTraSanPhamDaTonTaiGioHang(List<GioHang> listGioHang, int idSanPham, int idSize, int idMau) {
        for (int i = 0; i < listGioHang.size(); i++) {
            if (listGioHang.get(i).getIdSanPham() == idSanPham && listGioHang.get(i).getIdSize() == idSize && listGioHang.get(i).getIdMau() == idMau) {
                return i;
            }
        }
        return -1;
    }

    @GetMapping("/laysoluonggiohang")
    @ResponseBody
    public String laySoLuongGioHang(HttpSession httpSession) {

        if (null != httpSession.getAttribute("giohang")) {
            List<GioHang> listGioHang = (List<GioHang>) httpSession.getAttribute("giohang");
            return listGioHang.size() + "";
        }
        return "";
    }

    @GetMapping("/capnhatgiohang")
    @ResponseBody
    public void capNhatGioHang(HttpSession httpSession,
                               @RequestParam int idSanPham,
                               @RequestParam int idSize,
                               @RequestParam int idMau,
                               @RequestParam int soLuong) {

        if (null != httpSession.getAttribute("giohang")) {

            List<GioHang> listGioHang = (List<GioHang>) httpSession.getAttribute("giohang");

            int viTri = kiemTraSanPhamDaTonTaiGioHang(listGioHang, idSanPham, idSize, idMau);

            listGioHang.get(viTri).setSoLuong(soLuong);
        }
    }

    @GetMapping("/xoagiohang")
    @ResponseBody
    public void xoaGioHang(HttpSession httpSession,
                           @RequestParam int idSanPham,
                           @RequestParam int idSize,
                           @RequestParam int idMau) {

        if (null != httpSession.getAttribute("giohang")) {

            List<GioHang> listGioHang = (List<GioHang>) httpSession.getAttribute("giohang");

            int viTri = kiemTraSanPhamDaTonTaiGioHang(listGioHang, idSanPham, idSize, idMau);

            listGioHang.remove(viTri);
        }
    }
}
