<!--Author: Le Manh Cuong, Date: 24/08/2020 14:19PM -->
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src='<c:url value="js/jquery-3.5.1.min.js" />'></script>
    <link rel="stylesheet" href='<c:url value="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" />' />
    <script src='<c:url value="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" />'></script>
    <script src='<c:url value="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" />'></script>
    <script src='<c:url value="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" />'></script>
    <link href='<c:url value="fonts/Roboto.css" />' rel='stylesheet' />
    <link rel="stylesheet" href='<c:url value="css/animate.css" />' />
    <script src='<c:url value="js/wow.min.js" />'></script>
    <script>new WOW().init();</script>
    <script src='<c:url value="js/custom.js" />'></script>
    <link rel="stylesheet" type="text/css" href='<c:url value="css/styles.css" />' />
    <style>
        #header-chitiet{
            background-color: black;
            box-shadow: 0px 2px 5px #888888;
        }

        .mymenu{
            padding: 0;
            margin:0;
            list-style-type: none;
        }

        .mymenu li{
            font-size: 16px;
            font-weight: 400;
            padding: 4px 0px;
        }

        .container{
            margin-top: 20px;
        }

        .circle-giohang{
            text-align: center;
            position: absolute;
            width: 20px;
            height: 20px;
            border-radius: 40px;
            font-size: 11px;
            background-color: red;
            line-height: 22px;
            margin-top: -46px;
            margin-left: 25px;
        }
    </style>
    <title>Sản phẩm</title>
</head>
<body>
    <!-- header -->
    <div id="header-chitiet" class="container-fluid">
        <nav class="navbar navbar-expand-lg navbar-light bg-light" id="none-nav">
            <a class="navbar-brand" href="/"><img src="./images/logo.png" alt=""></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">TRANG CHỦ</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Sản phẩm
                        </a>
                        <ul class="dropdown-menu">
                            <c:forEach var="list" items="${danhmuc}">
                                <li><a href="/sanpham?idDanhMucSanPham=${list.getIdDanhMucSanPham()}&tenDanhMuc=${list.getTenDanhMuc()}">${list.getTenDanhMuc()}</a></li>
                                <li role="separator" class="dropdown-divider"></li>
                            </c:forEach>
                        </ul>
                        <%--                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">--%>
                        <%--                            <a class="dropdown-item" href="#">Action</a>--%>
                        <%--                            <a class="dropdown-item" href="#">Another action</a>--%>
                        <%--                            <div class="dropdown-divider"></div>--%>
                        <%--                            <a class="dropdown-item" href="#">Something else here</a>--%>
                        <%--                        </div>--%>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">DỊCH VỤ</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">LIÊN HỆ</a>
                    </li>
                    <!-- <li class="nav-item">
                      <a class="nav-link" href="#">Link</a>
                    </li> -->
                </ul>
                <ul class="navbar-nav navbar-right">
                    <c:choose>
                        <c:when test="${chucaidau != null}">
                            <li class="nav-item">
                                <a class="nav-link" style="margin-bottom: 5px;width: 32px;height: 32px;border-radius: 16px;background-color: chocolate;text-align: center;padding: 4px;margin-top: 5px;margin-right: 15px;">
                                    <span>${chucaidau}</span>
                                </a>
                            </li>
                        </c:when>
                        <c:otherwise>
                            <li class="nav-item"><a style="margin-bottom: 3px;" class="nav-link" href="/dangnhap">ĐĂNG NHẬP</a></li>
                        </c:otherwise>
                    </c:choose>

                    <li id="giohang">
                        <a href="/giohang">
                            <img width="40px" height="40px" src="./images/icon_shoppingcart.png" alt="">

                            <c:if test="${soluongsanphamgiohang > 0}">
                                <div class="circle-giohang">
                                    <span>${soluongsanphamgiohang}</span>
                                </div>
                            </c:if>

                            <c:if test="${soluongsanphamgiohang <= 0 || soluongsanphamgiohang == null}">
                                <div>
                                    <span>${soluongsanphamgiohang}</span>
                                </div>
                            </c:if>
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
    </div> <!-- end-header -->

    <div class="container">
        <div class="row">
            <div class="col-sm-2 col-md-2">
                <h3>Danh mục</h3>
                <ul class="mymenu">
                    <c:forEach var="list" items="${danhmuc}">
                        <li><a href="/sanpham?idDanhMucSanPham=${list.getIdDanhMucSanPham()}&tenDanhMuc=${list.getTenDanhMuc()}">${list.getTenDanhMuc()}</a></li>
                    </c:forEach>
                </ul>
            </div>

            <div class="col-sm-10 col-md-10">
                <div class="row"><h3>${tenDanhMuc}</h3></div>
                <div class="row">

                    <c:forEach var="list" items="${listSanPham}">
                        <div class="col-md-3 col-sm-6">
                            <a href="/chitiet?idsanpham=${list.getIdSanPham()}">
                                <div class="sanpham wow bounceIn">
                                    <img src='<c:url value="images/${list.getHinhSanPham()}" />' alt="" /><br>
                                    <span>${list.getTenSanPham()}</span><br>
                                    <span class="giatien">${list.getGiaTien()} VNĐ</span>
                                </div>
                            </a>
                        </div>
                    </c:forEach>
                </div>
            </div>
        </div>
    </div>

    <!-- footer -->
    <div id="footer" class="container-fluid">
        <div class="row">
            <div class="col-sm-4 col-md-4">
                <p><span class="title-footer">THÔNG TIN SHOP</span></p>
                <span>Shop Lê Cường là một thương hiệu thời trang đầy uy tín, luôn đảm bảo chất lượng sản phẩm tốt nhất cho khách hàng</span>
            </div>

            <div class="col-sm-4 col-md-4">
                <p><span class="title-footer">LIÊN HỆ</span></p>
                <p><span>Địa chỉ: Thị trấn Mỹ Lộc, huyện Mỹ Lộc, tỉnh Nam Định</span></p><br>
                <p><span>Email: lemanhcuong0498@gmail.com</span></p><br>
                <p><span>Số điện thoại: 0912345678</span></p>
            </div>

            <div class="col-sm-4 col-md-4">
                <p><span class="title-footer">GÓP Ý</span></p>
                <form action="">
                    <input type="text" placeholder="Email"><br>
                    <textarea name="" id="" cols="50" rows="4" placeholder="Nội dung"></textarea>
                    <button style="color: white;" class="material-primary-button" value="submit" name="">GỬI</button>
                </form>
            </div>
        </div>
    </div>
    <!-- end-footer -->
</body>
</html>