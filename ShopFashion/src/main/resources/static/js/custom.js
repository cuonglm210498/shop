$(document).ready(function() {
    $("#btnDangNhap").click(function () {
        var email = $("#email").val();
        var matkhau = $("#matkhau").val();
        $.ajax({
            url: '/api/kiemtradangnhap',
            type: 'post',
            data: {
                email: email,
                matkhau: matkhau
            },
            success: function (value) {
                if (value == "true") {
                    //chuyen page tu page dang nhap sang page trang chu neu dang nhap thanh cong
                    duongDanHienTai = window.location.href;
                    duongDan = duongDanHienTai.replace("dangnhap", "");
                    window.location = duongDan;
                } else {
                    $("#ketqua").text("Đăng nhập thất bại");
                }
            }
        })
    });

    $("#dangnhap").click(function () {
        $(this).addClass("actived");
        $("#dangky").removeClass("actived");
        $(".container-login-form").show();
        $(".container-signup-form").css("display", "none");
    });

    $("#dangky").click(function () {
        $(this).addClass("actived");
        $("#dangnhap").removeClass("actived");
        $(".container-login-form").hide();
        $(".container-signup-form").show();
    });

    $(".btnGioHang").click(function () {
        var idChiTietSanPham = $(this).attr("data-idChiTietSanPham");
        var idMauSanPham = $(this).closest("tr").find(".mau").attr("data-idmausanpham");
        var tenMau = $(this).closest("tr").find(".mau").text();
        var idSizeSanPham = $(this).closest("tr").find(".size").attr("data-idsizesanpham");
        var tenSize = $(this).closest("tr").find(".size").text();
        var tenSanPham = $("#tenSanPham").text();
        var giaTien = $("#giaTien").attr("data-value");
        var idSanPham = $("#tenSanPham").attr("data-idSanPham");
        var soLuong = $(this).closest("tr").find(".soluong").text();
        // alert(tenSanPham +" - "+ giaTien +" - "+tenMau +" - "+tenSize+" - "+idSanPham);
        $.ajax({
            url: '/api/themgiohang',
            type: 'get',
            data: {
                idSanPham: idSanPham,
                idSize: idSizeSanPham,
                idMau: idMauSanPham,
                tenSanPham: tenSanPham,
                giaTien: giaTien,
                tenMau: tenMau,
                tenSize: tenSize,
                soLuong: soLuong,
                idChiTietSanPham: idChiTietSanPham
            },
            success: function (value) {

            }
        }).done(function () {
            $.ajax({
                url: '/api/laysoluonggiohang',
                type: 'get',
                data: {
                    idSanPham: idSanPham,
                    idSize: idSizeSanPham,
                    idMau: idMauSanPham,
                    tenSanPham: tenSanPham,
                    giaTien: giaTien,
                    tenMau: tenMau,
                    tenSize: tenSize,
                    soLuong: soLuong
                },
                success: function (value) {
                    $("#giohang").find("div").addClass("circle-giohang");
                    $("#giohang").find("div").html("<span>" + value + "</span>");
                }
            })
        });
    })

    function tinhTong(){
        var giaTien = document.querySelectorAll(".giatien");
        var soluong =  document.querySelectorAll(".soluong-giohang");
        let tongTienSanPham = 0;
        let arr = [];
        for (let i=0 ; i< giaTien.length ; i++) {
            var giatien1 = parseInt(soluong[i].value) * parseInt(giaTien[i].getAttribute("data-value"));
            giaTien[i].textContent = (giatien1*1000).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
            if(giaTien[i].textContent.includes(",")){
                arr.push(giaTien[i].textContent.split(',').join(""));
            }else{
                arr.push(giaTien[i].textContent);
            }
        }
        for (let i=0;i<arr.length;i++){
            tongTienSanPham+=parseInt(arr[i]);
        }
        $("#tongtien").html(tongTienSanPham.toFixed(3).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,").split(".")[0]);
        return tongTienSanPham;
    }
    tinhTong();

    $('.soluong-giohang').on('keyup',function () {
        var tongTien = parseInt($(this).val()) * parseInt($(this).parent().next().attr('data-value'));
        $(this).parent().next().html((tongTien*1000).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
        tinhTong();
            var soLuong = $(this).val();
            var idSanPham = $(this).closest("tr").find(".tensanpham").attr("data-idSanPham");
            var idSizeSanPham = $(this).closest("tr").find(".size").attr("data-idSize");
            var idMauSanPham = $(this).closest("tr").find(".mau").attr("data-idMau");
        $.ajax({
            url: '/api/capnhatgiohang',
            type: 'get',
            data: {
                idSanPham: idSanPham,
                idSize: idSizeSanPham,
                idMau: idMauSanPham,
                soLuong: soLuong
            },
            success: function (value) {
            }
        })
    });
    //Xu ly in ra tong tien tat ca san pham trong gio hang (trang gio hang)
    // ganTongTienGioHang();
    //
    // function ganTongTienGioHang(isEventChange){
    //
    //     var tongTienSanPham = 0;
    //     $(".giatien").each(function () {
    //         var giatien = $(this).text();
    //         var soLuong = $(this).closest("tr").find(".soluong-giohang").val();
    //         var tongTien = parseInt(giatien) * soLuong;
    //
    //         var format = parseFloat(tongTien).toFixed(3).replace(/(\d)(?=(\d{3})+\.)/g, "$1.").toString();
    //
    //         // if (isEventChange){
    //         //     $(this).html(format);
    //         // }
    //         tongTienSanPham = tongTienSanPham + tongTien;
    //
    //         var formatTongTien = tongTienSanPham.toFixed(3).replace(/(\d)(?=(\d{3})+\.)/g, "$1.").toString();
    //         $(this).html(format+"");
    //         $("#tongtien").html(formatTongTien+"");
    //     })
    // }
    //
    // //Xu ly in ra tong tien cua tung san pham khi thay doi so luong san pham trong gio hang (trang gio hang)
    // $(".soluong-giohang").change(function () {
    //
    //     //cap nhat lai so luong gio hang
    //     var soLuong = $(this).val();
    //     var idSanPham = $(this).closest("tr").find(".tensanpham").attr("data-idSanPham");
    //     var idSizeSanPham = $(this).closest("tr").find(".size").attr("data-idSize");
    //     var idMauSanPham = $(this).closest("tr").find(".mau").attr("data-idMau");
    //
    //     $.ajax({
    //         url: '/api/capnhatgiohang',
    //         type: 'get',
    //         data: {
    //             idSanPham: idSanPham,
    //             idSize: idSizeSanPham,
    //             idMau: idMauSanPham,
    //             soLuong: soLuong
    //         },
    //         success: function (value) {
    //
    //             ganTongTienGioHang(true);
    //         }
    //     })
    //
    // })

    //Xoa san pham co trong gio hang
    $(".xoa-giohang").click(function () {
        var self = $(this);
        var idSanPham = $(this).closest("tr").find(".tensanpham").attr("data-idSanPham");
        var idSizeSanPham = $(this).closest("tr").find(".size").attr("data-idSize");
        var idMauSanPham = $(this).closest("tr").find(".mau").attr("data-idMau");
        
        $.ajax({
            url: '/api/xoagiohang',
            type: 'get',
            data: {
                idSanPham: idSanPham,
                idSize: idSizeSanPham,
                idMau: idMauSanPham,
            },
            success: function (value) {
                self.closest("tr").remove();
                tinhTong();
            }
        })
    })

})